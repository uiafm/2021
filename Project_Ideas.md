![aa](../images/qjam2021header.jpg)

Here is a list of QJamming ideas :)


| Project Idea | Keywords |
| ------------ | -------- | 
| [Interactive Bloch Sphere](project_ideas/Interactive_Bloch_Sphere.md) | QEducation, QGames, QProgramming |
| [Discord Bots for Quantum](project_ideas/Discord_Bots_for_Quantum.md) | OpenQEcoSystem, QEducation, QGames, QProgramming |
| [Integrate_Quantum_Composer](project_ideas/Integrate_Quantum_Composer.md) | QEducation, QProgramming, QSoftware, OpenQEcoSystem |
| [Benchmarking_Error_Mitigation_techniques](project_ideas/Benchmarking_Error_Mitigation.md) | QImplementaion, QProgramming, QSoftware, OpenQEcoSystem |
| [QTalk_Ideas](project_ideas/QTalk_Ideas.md) | QEducation, QShort, OpenQEcosystem, QOutreach |
| [Animation_for_QFT](project_ideas/Animation_for_QFT.md) | QArt, QAnimation, QEducation, QJunior |
| [Comics_for_Shors_Algorithm](project_ideas/Comics_for_Shors_Algorithm.md) | QArt, QComics, QEducation, QJunior |
| [Interactive_Jupyter_Notebooks](project_ideas/Interactive_Jupyter_Notebooks.md ) | QEducation, QGames, QFreeAccess, QProgramming, QJunior |
| [Short_for_Entaglement](project_ideas/Short_for_Entaglement.md) | QArt, QShort, QComics |
| [Story_for_Superposition](project_ideas/Story_for_Superposition.md) | QArt, QComics, QJunior |
