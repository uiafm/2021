# Story for Superposition

**Keywords:** QArt, QComics, QJunior

## Motivation

Young people are ready to learn new concepts including quantum ones. 

One of the best pedagogical strategies for students would be using stories.

"Alice's Adventures in Wonderland" would be an inspiring example.

## An idea

A story of a couple of electrons to come together for implementing a very simple computational task. 
But, they are very unstable, and there are several environmental factors to prevent them to do that.

##

_Prepared by Abuzer Yakaryilmaz_