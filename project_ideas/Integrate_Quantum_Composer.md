# Integrating QEngine quantum composer in Jupyter

**Keywords:** QEducation, QProgramming, QSoftware, OpenQEcoSystem

## Motivation
Use the Quantum Composer (https://www.quatomic.com/composer/) and integrate the same for added visualization of behind the scenes abstration of physics concepts of quantum information

## Some Ideas

Create pluggable jupyter notebook extensions for wasy use of the quantum composer

Modify current educational materials like QBronze to implement the quantum composer for exercises

##

