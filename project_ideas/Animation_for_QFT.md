# Animation for QFT

**Keywords:** QArt, QAnimation, QEducation, QJunior

## Motivation

Quantum Fourier transform is one of the fundamental concepts in quantum computing, but teaching QFT in a pedagogical way is always challenging. 

Different mediums or tools can be used.

The visualization of QFT is always helpful, and we can go one more step further and animate it.

##

_Prepared by Abuzer Yakaryilmaz_